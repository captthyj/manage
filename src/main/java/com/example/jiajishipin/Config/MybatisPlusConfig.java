package com.example.jiajishipin.Config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 *@ClassName MybatisPlusConfig
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 21:53
 *@Version 1.0
 **/

@Configuration
@MapperScan("com.example.jiajishipin.mapper")
public class MybatisPlusConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        PaginationInterceptor page = new PaginationInterceptor();
        page.setDialectType("mysql");
        return page;
    }
}
