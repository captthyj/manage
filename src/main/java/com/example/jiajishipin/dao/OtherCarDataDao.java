package com.example.jiajishipin.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@TableName(value = "other_car_data", autoResultMap = true)
public class OtherCarDataDao {
/**
 *@ClassName OtherCarDataDao
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 18:03
 *@Version 1.0
 **/
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String number;

    private String filePath;//带上时分秒

    private Integer year;

    private Integer month;

    private Integer day;

    @TableField(typeHandler = FastjsonTypeHandler.class)
    private Map<String, String> cameraDatas;

    private String scannerImage;

    private LocalDateTime gmtCreated;

    private LocalDateTime gmtModified;
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<CarInfos> carInfosList;

//    private String applyPart;
//
//    private String name;
//
//    private String unit;
//
//    private String count;
//
//    private String goodsLocation;
//
//    private String isChargeGoods;
//
//    private String notes;
    @Data
    private static class CarInfos {
        private String applyPart;

        private String name;

        private String unit;

        private String count;

        private String goodsLocation;

        private String isChargeGoods;

        private String notes;
    }
}
