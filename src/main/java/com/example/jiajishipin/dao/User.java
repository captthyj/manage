package com.example.jiajishipin.dao;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data

public class User {
    /*
     * @Author Hyj
     * @Description //TODO
     * @Date 2023/2/14 19:54
     * @Param
     * @return
     **/

    private String id;
    private String userName;
    private String password;
    private String telephone;
    private String sex;
}
