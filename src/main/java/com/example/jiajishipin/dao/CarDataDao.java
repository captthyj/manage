package com.example.jiajishipin.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@TableName(value = "car_data", autoResultMap = true)
public class CarDataDao {
/**
 *@ClassName CarDataDao
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 18:03
 *@Version 1.0
 **/
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String number;

    private String filePath;//带上时分秒

    private Integer year;

    private Integer month;

    private Integer day;

    @TableField(typeHandler = FastjsonTypeHandler.class)
    private Map<String, String> cameraDatas;

    private String scannerImage;

    private LocalDateTime gmtCreated;

    private LocalDateTime gmtModified;
}
