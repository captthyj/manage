package com.example.jiajishipin.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "image_date", autoResultMap = true)
public class ImageDataDao {
/**
 *@ClassName ImageDataDao
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 18:00
 *@Version 1.0
 **/
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long carDataId;

    private byte[] data;

    private String number;

    private String cameraNumber;

    private String filePath;

    private LocalDateTime gmtCreated;

    private LocalDateTime gmtModified;
}
