package com.example.jiajishipin;

import com.example.jiajishipin.Impl.TaskServiceImpl;
import com.example.jiajishipin.redis.RedisTemplateUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class JiajishipinApplication {
    public static void main(String[] args) {
        SpringApplication.run(JiajishipinApplication.class, args);
    }

}
