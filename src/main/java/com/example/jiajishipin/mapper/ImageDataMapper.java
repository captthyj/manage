package com.example.jiajishipin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jiajishipin.dao.ImageDataDao;

public interface ImageDataMapper extends BaseMapper<ImageDataDao> {
}
