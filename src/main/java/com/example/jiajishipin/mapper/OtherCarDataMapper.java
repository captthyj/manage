package com.example.jiajishipin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jiajishipin.dao.CarDataDao;
import com.example.jiajishipin.dao.OtherCarDataDao;

public interface OtherCarDataMapper extends BaseMapper<OtherCarDataDao> {
}
