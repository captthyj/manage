package com.example.jiajishipin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jiajishipin.dao.CarDataDao;

public interface CarDataMapper extends BaseMapper<CarDataDao> {
}
