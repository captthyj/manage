package com.example.jiajishipin.redis;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class RedisTemplateUtil {
/**
 *@ClassName RedisTemplate
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/25 13:57
 *@Version 1.0
 **/
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    public void setKeyValue(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }
    public String getKeyValue(String key) {
        return redisTemplate.opsForValue().get(key);
    }
    public void setKeyValueTime(String key, String value, Long timeout, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key,value,timeout, timeUnit);
    }
    public void deleteKey(String key) {
        redisTemplate.delete(key);
    }
    public boolean getKey(String key) {
        return redisTemplate.hasKey(key);
    }
}
