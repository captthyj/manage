package com.example.jiajishipin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.awt.image.BufferedImage;

@Data
@AllArgsConstructor
public class ImageContainer {
/**
 *@ClassName ImageContainer
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/25 16:00
 *@Version 1.0
 **/
    private BufferedImage image;
    private String imageName;
}
