package com.example.jiajishipin.controller;

import com.example.jiajishipin.service.UserService;
import com.example.jiajishipin.utils.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@AllArgsConstructor
@RestController
@Validated
@RequestMapping("/admin")
public class AdminController {
    /**
     *@ClassName AdminController
     *@Description TODO
     *@Author Hyj
     *@Date 2023/4/11 10:44
     *@Version 1.0
     **/
    @Autowired
    UserService userService;

    @PostMapping("/login")
    public ApiResult login(@RequestParam(required = true)String userName, @RequestParam(required = true) String password){
        return userService.login(userName,password);
    }
    @PostMapping("/addUser")
    public ApiResult addUser(@RequestParam(required = true)String userName, @RequestParam(required = true) String password){
        return userService.addUser(userName,password,"","");
    }

    @PostMapping("/changePassword")
    public ApiResult changePassword(@RequestParam(required = true)String userName, @RequestParam(required = true) String oldPassword, @RequestParam(required = true) String newPassword){
        return userService.changePass(userName,oldPassword,newPassword);
    }
}

