package com.example.jiajishipin.controller;

import com.example.jiajishipin.redis.RedisTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@RequestMapping("/admin")
public class TestController {
/**
 *@ClassName TestController
 *@Description TODO
 *@Author Hyj
 *@Date 2024/3/7 23:37
 *@Version 1.0
 **/
    @Autowired
    RedisTemplateUtil redisTemplateUtil;
    @GetMapping("/test")
    public String test() {

        System.out.println(redisTemplateUtil.getKeyValue("test"));
        return "1";
    }
}
