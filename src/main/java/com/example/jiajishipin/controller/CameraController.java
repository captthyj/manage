package com.example.jiajishipin.controller;

import com.example.jiajishipin.service.CameraService;
import com.example.jiajishipin.utils.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *@ClassName CameraController
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/27 22:24
 *@Version 1.0
 **/
@RestController
@Validated
@CrossOrigin
@RequestMapping("/camera")
public class CameraController {
    @Autowired
    CameraService cameraService;

    @GetMapping("/getCarData")
    public ApiResult getCarData() {
        return cameraService.getCarData();
    }

    @GetMapping("/doGetCameraDataByPerson")
    public ApiResult doGetCameraDataByPerson(@RequestParam(required = false) String number){
        return cameraService.doGetCameraDataByPerson(number);
    }
    @GetMapping("/removeCar")
    public ApiResult removeCar(){
        cameraService.removeCar();
        return ApiResult.ofSuccess();
    }
}
