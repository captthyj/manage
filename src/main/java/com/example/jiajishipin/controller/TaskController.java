package com.example.jiajishipin.controller;

import com.example.jiajishipin.service.CameraService;
import com.example.jiajishipin.service.TaskService;
import com.example.jiajishipin.utils.ApiResult;

import cn.hutool.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *@ClassName TaskController
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/27 22:19
 *@Version 1.0
 **/

@RestController
@Validated
@CrossOrigin
@RequestMapping("/task")
public class TaskController {
    @Autowired
    TaskService taskService;
    @Autowired
    CameraService cameraService;
    @GetMapping("/cameraOne")
    public ApiResult cameraOne(@RequestParam String number) {
        taskService.cameraOneEvent(number);
        return ApiResult.ofSuccess();
    }
    @GetMapping("/cameraTwo")
    public ApiResult cameraTwo(@RequestParam String number) {
        taskService.cameraTwoEvent(number);
        return ApiResult.ofSuccess();
    }

    @GetMapping("/initSubscript")
    public ApiResult subscriptTask(){
        try {
            if(cameraService.subscriptTask()) {
                return ApiResult.ofSuccess();
            } else {
                throw new Exception("注册失败");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return ApiResult.ofFail(500, e.getMessage());
        }
    }

    @PostMapping("/lineCrushingEvent")
    public ApiResult lineCrushingEvent(@RequestBody JSONObject data){
        taskService.lineCrushingEvent(data);
        return ApiResult.ofSuccess();
    }
    @GetMapping("/getDelayTime")
    public ApiResult getDelayTime(){
        return taskService.getDelayTime();
    }
    @GetMapping("/setDelayTime")
    public ApiResult setDelayTime(@RequestParam Integer delayTime){
        return taskService.setDelayTime(delayTime);
    }
}
