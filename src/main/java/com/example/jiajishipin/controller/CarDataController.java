package com.example.jiajishipin.controller;

import com.example.jiajishipin.dao.CarDataDao;
import com.example.jiajishipin.dao.OtherCarDataDao;
import com.example.jiajishipin.service.CarDataService;
import com.example.jiajishipin.service.OtherCarDataService;
import com.example.jiajishipin.utils.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 *@ClassName CarDataController
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/27 22:19
 *@Version 1.0
 **/

@RestController
@Validated
@CrossOrigin
@RequestMapping("/carData")
public class CarDataController {
    @Autowired
    CarDataService carDataService;
    @Autowired
    OtherCarDataService otherCarDataService;
    @GetMapping("/getCarData")
    public ApiResult getCarData(@RequestParam(required = false) String number, @RequestParam int currentPage, @RequestParam int pageSize,
                                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam(required = false) LocalDateTime fromDate,
                                @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam(required = false) LocalDateTime toDate) {
        return carDataService.getCarDataByNumber(fromDate, toDate, number, currentPage, pageSize);
    }
    @PostMapping("/saveCarData")
    public ApiResult saveCarData(@RequestBody  CarDataDao carDataDao) throws IOException {
        try {
            if(carDataService.saveCarData(carDataDao)) {
                return ApiResult.ofSuccess();
            }
            return ApiResult.ofFail(400, "保存出错");
        }catch (Exception e) {
            e.printStackTrace();
            return ApiResult.ofFail(400, e.getMessage());
        }
    }
    @PostMapping("/saveOtherCarData")
    public ApiResult saveOtherCarData(@RequestBody OtherCarDataDao carDataDao) throws IOException {
        try {
            if(otherCarDataService.saveOtherCarData(carDataDao)) {
                return ApiResult.ofSuccess();
            }
            return ApiResult.ofFail(400, "保存出错");
        }catch (Exception e) {
            e.printStackTrace();
            return ApiResult.ofFail(400, e.getMessage());
        }
    }
    @GetMapping("/getOtherCarData")
    public ApiResult getOtherCarData(@RequestParam int currentPage, @RequestParam int pageSize,
                                     @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam(required = false) LocalDateTime fromDate,
                                     @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam(required = false) LocalDateTime toDate,
                                     @RequestParam(required = false)String applyPart,@RequestParam(required = false)String name,
                                     @RequestParam(required = false)String unit, @RequestParam(required = false)String count,
                                     @RequestParam(required = false)String goodsLocation,@RequestParam(required = false)String isChargeGoods,
                                     @RequestParam(required = false)String notes) {
        return otherCarDataService.getOtherCarData(fromDate, toDate, applyPart, name, unit, count, goodsLocation, isChargeGoods, notes, currentPage, pageSize);
    }
}
