package com.example.jiajishipin.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.alibaba.fastjson.JSON;
import com.example.jiajishipin.service.CameraService;
import com.example.jiajishipin.utils.ObjectEncoder;

import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@CrossOrigin
@ServerEndpoint(value="/ws/getCarData", encoders = {ObjectEncoder.class})
public class WebSocketHandler {
    @Autowired
    CameraService cameraService;

    /**
     * 线程安全的无序的集合
     */
    private static final CopyOnWriteArraySet<Session> SESSIONS = new CopyOnWriteArraySet<>();


    @OnOpen
    public void onOpen(Session session) {
        try {
            SESSIONS.add(session);
            log.info("【WebSocket消息】有新的连接，总数为：" + SESSIONS.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void onClose(Session session) {
        try {
            SESSIONS.remove(session);
            log.info("【WebSocket消息】连接断开，总数为：" + SESSIONS.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnMessage
    public void onMessage(String message) {
        log.info("【WebSocket消息】收到客户端消息：" + message);
    }

    @Scheduled(fixedRate = 2000)
    public void sendAllMessage() {
        for (Session session : SESSIONS) {
            try {
                if (session.isOpen()) {
                    session.getBasicRemote().sendObject(cameraService.getCarData());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Scheduled(fixedRate = 20000)
    public void printSessions(){
        log.info("websocket sessions: ");
        for (Session session : SESSIONS) {
            try {
                if (session.isOpen()) {
                    log.info(session.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}