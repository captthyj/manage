package com.example.jiajishipin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jiajishipin.utils.ApiResult;
import com.example.jiajishipin.dao.CarDataDao;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 *@ClassName CarDataService
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 18:08
 *@Version 1.0
 **/
public interface CarDataService extends IService<CarDataDao> {
    public ApiResult getCarDataByNumber(LocalDateTime fromDate, LocalDateTime toDate,String number, int currentPage, int pageSize);
    public Boolean saveCarData(CarDataDao carDataDao) throws IOException;
}
