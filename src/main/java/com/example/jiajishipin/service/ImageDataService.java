package com.example.jiajishipin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jiajishipin.dao.ImageDataDao;
import com.example.jiajishipin.utils.ApiResult;

public interface ImageDataService extends IService<ImageDataDao> {
    public ApiResult getImageDataByNumber(String number, int currentPage, int pageSize);
    public ApiResult getImageDataByCarDataId(Long catDataId, int currentPage, int pageSize);
    public ApiResult getImageDataByFilePath(String filePath, int currentPage, int pageSize );
}
