package com.example.jiajishipin.service;

import com.example.jiajishipin.utils.ApiResult;

import java.util.List;
import java.util.Map;

public interface CameraService {
    Map<String, String> getCameraList(String userId) throws Exception;
    String getCameraPic(String userId, String domainId, String cameraIndexCode) throws Exception;
    Boolean registerEvent(String userId, List<Integer> eventTypes, String dest) throws Exception;
    public void doGetCameraData(String number, Integer delayTime);
    public ApiResult doGetCameraDataByPerson(String number);
    public ApiResult getCarData();
    public void removeCar();
    public boolean subscriptTask() throws Exception;

}
