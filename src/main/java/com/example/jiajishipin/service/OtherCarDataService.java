package com.example.jiajishipin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jiajishipin.dao.CarDataDao;
import com.example.jiajishipin.dao.OtherCarDataDao;
import com.example.jiajishipin.utils.ApiResult;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 *@ClassName CarDataService
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 18:08
 *@Version 1.0
 **/
public interface OtherCarDataService extends IService<OtherCarDataDao> {
    public ApiResult getOtherCarData(LocalDateTime fromDate, LocalDateTime toDate, String applyPart,String name, String unit, String count, String goodsLocation,String isChargeGoods, String notes, int currentPage, int pageSize);
    public Boolean saveOtherCarData(OtherCarDataDao carDataDao) throws IOException;
}
