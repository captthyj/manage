package com.example.jiajishipin.service;

import com.example.jiajishipin.utils.ApiResult;

public interface UserService {
    /**
     *@ClassName IUserService
     *@Description TODO
     *@Author Hyj
     *@Date 2024/3/11 21:45
     *@Version 1.0
     **/
    public ApiResult login(String userName, String password);

    public ApiResult addUser(String userName, String password, String tel, String sex);

    public ApiResult changePass(String userName, String oldPassword, String newPassword);
}
