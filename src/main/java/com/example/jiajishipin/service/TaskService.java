package com.example.jiajishipin.service;

import cn.hutool.json.JSONObject;
import com.example.jiajishipin.utils.ApiResult;

public interface TaskService {
/**
 *@ClassName TaskService
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/25 12:04
 *@Version 1.0
 **/
    public void saveData(String number);
    public void cameraOneEvent(String number);
    public void cameraTwoEvent(String number);
    public void lineCrushingEvent(JSONObject data);
    public void init();
    public ApiResult getDelayTime();
    public ApiResult setDelayTime(Integer delayTime);
}
