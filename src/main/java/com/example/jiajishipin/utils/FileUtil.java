package com.example.jiajishipin.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

@Component
public class FileUtil {
/**
 *@ClassName FileUtil
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/13 22:12
 *@Version 1.0
 **/
    @Value("${file.path}")
    public static String filePath;
    public static Path getFilePath(String number){
        LocalDate currentDate = LocalDate.now();

        int year = currentDate.getYear();
        int month = currentDate.getMonthValue();
        int day = currentDate.getDayOfMonth();
        String directoryPath = filePath + "/" + String.valueOf(year) + "/" + String.valueOf(month) +"/" + String.valueOf(day) + "/" + number;
        Path path = Paths.get(directoryPath);

        try {
            // 如果目录不存在，则创建它
            if (!Files.exists(path)) {
                Files.createDirectories(path);
                System.out.println("Directory created: " + path);
            } else {
                System.out.println("Directory already exists: " + path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path.toAbsolutePath();
    }
    public static Boolean saveFile(String number, BufferedImage image) {
        try {
            Path path = getFilePath(number + LocalDate.now());
            // 指定保存图片的路径和文件名
            File outputfile = path.toFile();

            // 将BufferedImage对象写入文件
            ImageIO.write(image, "jpg", outputfile);

            System.out.println("Image downloaded and saved successfully!");
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
