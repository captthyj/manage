package com.example.jiajishipin.utils;

import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.jiajishipin.enums.CommonErrorEnum;


import java.io.Serializable;
import java.text.MessageFormat;

/*
 * @Author Hyj
 * @Description //TODO 
 * @Date  2023/2/18 16:50
 **/
public class ApiResult<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 成功数据
     */
    private T data;

    /**
     * 状态码
     */
    private int code = HttpStatus.HTTP_OK;

    /**
     * 描述
     */
    private String msg;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ApiResult() {

    }

    public ApiResult(T data) {
        this.data = data;
    }

    public ApiResult(T data, String msg) {
        this.data = data;
        this.msg = msg;
    }

    public ApiResult(T data, int code, String msg) {
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public static <T> ApiResult<T> ofSuccess() {
        return new ApiResult<>(null, 200, "操作成功!");
    }

    public static <T> ApiResult<T> ofSuccess(T data) {
        return new ApiResult<>(data, 200, "操作成功!");
    }

    public static <T> ApiResult<T> ofSuccess(String message, T data) {
        return new ApiResult<>(data, 200, message);
    }

    public static <T> ApiResult<T> ofFail(int code, String msg) {
        return new ApiResult<>(null, code, msg);

    }

    public static ApiResult errorWithArgs(CommonErrorEnum status, Object... args) {
        return new ApiResult(status.getCode(), MessageFormat.format(status.getMessage(), args));
    }

    public static <T> ApiResult<T> ofFail(int code, String msg, T data) {
        return new ApiResult<>(data, code, msg);
    }

    public static <T> ApiResult<T> ofFail(CommonErrorEnum resultEnum) {
        return new ApiResult<>(null, resultEnum.getCode(), resultEnum.getMessage());
    }

    public static <T> ApiResult<T> ofFail(CommonErrorEnum resultEnum, T data) {
        return new ApiResult<>(data, resultEnum.getCode(), resultEnum.getMessage());
    }

    /**
     * 获取 json
     */
    public String buildResultJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", this.code);
        jsonObject.put("msg", this.msg);
        jsonObject.put("data", this.data);
        return JSON.toJSONString(jsonObject, SerializerFeature.DisableCircularReferenceDetect);
    }

    public Boolean checkResult() {
        return this.code == CommonErrorEnum.SUCCESS.getCode();
    }


    @Override
    public String toString() {
        return "Result{" +
                ", data=" + data +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}