package com.example.jiajishipin.utils;
import lombok.Data;

import java.util.List;
/**
 * mongoDb分页对象
 */
@Data
public class PageResult<T> {
    private Integer currentPage;
    //每页的数量
    private Integer pageSize;
    //总共的条数
    private Long total;
    //总共的页数
    private Integer pages;
    //实体类集合
    private List<T> list;



    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

}



