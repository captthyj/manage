package com.example.jiajishipin.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeUtil {
/**
 *@ClassName TimeUtil
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 21:31
 *@Version 1.0
 **/
    public static String getTime(LocalDateTime currentDateTime) {

        // 创建一个DateTimeFormatter对象，用于定义日期和时间的格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        // 使用formatter将LocalDateTime对象格式化为字符串
        return currentDateTime.format(formatter);
    }
}
