package com.example.jiajishipin.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jiajishipin.dao.CarDataDao;
import com.example.jiajishipin.mapper.ImageDataMapper;
import com.example.jiajishipin.service.ImageDataService;
import com.example.jiajishipin.dao.ImageDataDao;
import com.example.jiajishipin.utils.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *@ClassName ImageDataServiceImpl
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 21:33
 *@Version 1.0
 **/
@Service
public class ImageDataServiceImpl extends ServiceImpl<ImageDataMapper, ImageDataDao> implements ImageDataService {

    @Autowired
    ImageDataMapper imageDataMapper;

    @Override
    public ApiResult getImageDataByNumber(String number, int currentPage, int pageSize) {
        QueryWrapper<ImageDataDao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(ImageDataDao::getNumber, number);
        IPage<ImageDataDao> page = new Page<>(currentPage, pageSize);
        imageDataMapper.selectPage(page, queryWrapper);
        return ApiResult.ofSuccess(page);
    }

    @Override
    public ApiResult getImageDataByCarDataId(Long catDataId, int currentPage, int pageSize) {
        QueryWrapper<ImageDataDao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(ImageDataDao::getCarDataId, catDataId);
        IPage<ImageDataDao> page = new Page<>(currentPage, pageSize);
        imageDataMapper.selectPage(page, queryWrapper);
        return ApiResult.ofSuccess(page);
    }

    @Override
    public ApiResult getImageDataByFilePath(String filePath, int currentPage, int pageSize) {
        QueryWrapper<ImageDataDao> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(ImageDataDao::getFilePath, filePath);
        IPage<ImageDataDao> page = new Page<>(currentPage, pageSize);
        imageDataMapper.selectPage(page, queryWrapper);
        return ApiResult.ofSuccess(page);
    }

}
