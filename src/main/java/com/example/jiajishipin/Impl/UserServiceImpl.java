package com.example.jiajishipin.Impl;

import com.example.jiajishipin.enums.CommonErrorEnum;
import com.example.jiajishipin.service.UserService;

import com.example.jiajishipin.utils.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    /**
     *@ClassName UserServiceImpl
     *@Description TODO
     *@Author Hyj
     *@Date 2023/4/11 10:50
     *@Version 1.0
     **/


    @Override
    public ApiResult login(String userName, String password) {
        try {
            return ApiResult.ofSuccess();
        } catch (Exception e) {
            return ApiResult.ofFail(CommonErrorEnum.EXCEPTION,e.getMessage());
        }
    }

    @Override
    public ApiResult addUser(String userName, String password, String tel, String sex) {
        try {
            return ApiResult.ofSuccess();
        } catch (Exception e) {
            return ApiResult.ofFail(CommonErrorEnum.EXCEPTION,e.getMessage());
        }
    }

    @Override
    public ApiResult changePass(String userName, String oldPassword, String newPassword) {
        try {

            return ApiResult.ofSuccess();
        } catch (Exception e) {
            return ApiResult.ofFail(CommonErrorEnum.EXCEPTION,e.getMessage());
        }
    }

    public String getMd5(String s) {
        return DigestUtils.md5Hex(s);
    }
}
