
package com.example.jiajishipin.Impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.jiajishipin.dao.CarDataDao;
import com.example.jiajishipin.redis.RedisTemplateUtil;
import com.example.jiajishipin.service.CameraService;
import com.example.jiajishipin.utils.ApiResult;
import com.example.jiajishipin.utils.RedisUtil;
import com.hikvision.artemis.sdk.ArtemisHttpUtil;
import com.hikvision.artemis.sdk.config.ArtemisConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * @description: 监控点相关封装接口
 * @author: fx
 * @time: 2024/5/22 17:03
 */
@Service
public class CameraServiceImpl implements CameraService {

    @Value("${app.artemis.host}")
    private String artemisHost;
    @Value("${app.artemis.context}")
    private String artemisContext;
    @Value("${app.artemis.ak}")
    private String ak;
    @Value("${app.artemis.sk}")
    private String sk;
    @Value("${app.artemis.api-protocol}")
    private String apiProtocol;
    @Value("${userid}")
    private String userId;
    @Value("${pre-path}")
    private String prePath;
    @Value("${task-path-prefix}")
    private String taskPathPrefix;

    @Value("${front-detect-only-camera-id}")
    private String frontDetectOnlyCameraId;

    @Value("${end-detect-only-camera-id}")
    private String endDetectOnlyCameraId;

    @Autowired
    RedisUtil redisUtil;
    @Autowired
    RedisTemplateUtil redisTemplateUtil;
    private static  final Logger logger = LoggerFactory.getLogger(CameraServiceImpl.class);

    public CameraServiceImpl(){
    }
    public ConcurrentHashMap<String, CarDataDao>  concurrentHashMap = new ConcurrentHashMap();

    /**
     * 获取全部监控点列表 -- 对应接口：分页获取监控点资源
     * @param userId 接口是这么写的，我也不知道为什么要传个user id
     * @return Map of < cameraId:String , cameraId:String > // 之前 key 是 cameraName ，但是会重复
     * @throws Exception 来自 ArtemisHttpUtil 的异常
     */
    public Map<String, String> getCameraList(String userId) throws Exception {
        ArtemisConfig artemisConfig = new ArtemisConfig(artemisHost, ak, sk);
        final String url = artemisContext + "/api/resource/v1/cameras"; // 接口地址

        Map<String, String> path = new HashMap<String, String>() {{
            put(apiProtocol, url); //Key 为协议，Value 为接口地址
        }};
        JSONObject body = new JSONObject(){{
            put("pageNo", 1);
            put("pageSize", 1000);
        }};
        String bodyStr = JSONObject.toJSONString(body);
        Map<String, String> header = new HashMap<String, String>(){{
           put("userId", userId);
        }};
        String res = ArtemisHttpUtil.doPostStringArtemis(artemisConfig, path, bodyStr,
                null, "application/json", "application/json",
                header);
        JSONObject resJsonObj = JSONObject.parseObject(res);
        JSONArray resList = resJsonObj.getJSONObject("data").getJSONArray("list");

        return resList.stream()
                .map(obj -> (JSONObject) obj)
                .collect(Collectors.toMap(
                        camera -> camera.getString("cameraName"),
                        camera -> camera.getString("cameraIndexCode"),
                        (item1, item2) -> item2
                ));
    }

    /**
     * 手动抓图 -- 对应同名接口
     * @param userId 用户id
     * @param domainId 网域唯一标识
     * @param cameraIndexCode 监控点id
     * @return 图片Url
     * @throws Exception 来自 ArtemisHttpUtil 的异常
     */
    public String getCameraPic(String userId, String domainId, String cameraIndexCode) throws Exception {
        ArtemisConfig artemisConfig = new ArtemisConfig(artemisHost, ak, sk);
        final String url = artemisContext + "/api/video/v1/manualCapture"; // 接口地址

        Map<String, String> path = new HashMap<String, String>() {{
            put(apiProtocol, url); //Key 为协议，Value 为接口地址
        }};
        JSONObject body = new JSONObject(){{
            put("cameraIndexCode", cameraIndexCode);
        }};
        String bodyStr = JSONObject.toJSONString(body);
        Map<String, String> header = new HashMap<String, String>(){{
            put("userId", userId);
            put("domainId", domainId);
        }};

        String res = ArtemisHttpUtil.doPostStringArtemis(artemisConfig, path, bodyStr,
                null, "application/json", "application/json",
                header);
        JSONObject resJsonObj = JSONObject.parseObject(res);
        System.out.println(resJsonObj);
        return resJsonObj.getJSONObject("data").getString("picUrl");
    }

    /**
     * 注册订阅事件 -- 对应接口：按事件类型订阅事件
     * @param userId 用户id
     * @param eventTypes 事件id列表
     * @param dest 接受事件的回调地址
     * @return 注册是否成功
     * @throws Exception 来自 ArtemisHttpUtil 的异常
     */
    public Boolean registerEvent(String userId, List<Integer> eventTypes, String dest) throws Exception {
        ArtemisConfig artemisConfig = new ArtemisConfig(artemisHost, ak, sk);
        final String url = artemisContext + "/api/eventService/v1/eventSubscriptionByEventTypes"; // 接口地址

        Map<String, String> path = new HashMap<String, String>() {{
            put(apiProtocol, url); //Key 为协议，Value 为接口地址
        }};
        JSONObject body = new JSONObject(){{
            put("eventTypes", eventTypes);
            put("eventDest", dest);
            put("subType", 2);
        }};
        String bodyStr = JSONObject.toJSONString(body);
        Map<String, String> header = new HashMap<String, String>(){{
            put("userId", userId);
        }};

        String res = ArtemisHttpUtil.doPostStringArtemis(artemisConfig, path, bodyStr,
                null, "application/json", "application/json",
                header);
        JSONObject resJsonObj = JSONObject.parseObject(res);
        return resJsonObj.get("code").equals("0");
    }

    public boolean subscriptTask() throws Exception{
        return registerEvent(
            userId, 
            Arrays.asList(131622)
            , taskPathPrefix+"/task/lineCrushingEvent");
    }
    
    /**
     * 获取监控点视频预览流
     * @param userId 用户id
     * @param domainId 网域id
     * @param cameraIndexCode 监控点id
     * @param protocol 以下值之一：["hik"(default), "rtsp", "rtmp", "hls"]
     * @param expend 无需求则传null jsonobject形式的扩展选项 详见OpenApiDoc-附录F expand扩展内容说明
     * @param streamForm 无需求则传null 输出码流转封装格式 ["ps", "rtp"(default)]
     * @return 视频流url
     * @throws Exception 来自 ArtemisHttpUtil 的异常
     */
    public String getCameraVideoStream(String userId, String domainId, String cameraIndexCode,
                                        String protocol, JSONObject expend, String streamForm) throws Exception {
        ArtemisConfig artemisConfig = new ArtemisConfig(artemisHost, ak, sk);
        final String url = artemisContext + " /api/video/v2/cameras/previewURLs"; // 接口地址

        Map<String, String> path = new HashMap<String, String>() {{
            put(apiProtocol, url); //Key 为协议，Value 为接口地址
        }};
        JSONObject body = new JSONObject(){{
            put("cameraIndexCode", cameraIndexCode);
            put("protocol", protocol);
            if(expend!=null){
                put("expend", expend);
            }
            if(streamForm!=null){
                put("streamform", streamForm);
            }
        }};
        String bodyStr = JSONObject.toJSONString(body);
        Map<String, String> header = new HashMap<String, String>(){{
            put("userId", userId);
            put("domainId", domainId);
        }};

        String res = ArtemisHttpUtil.doPostStringArtemis(artemisConfig, path, bodyStr,
                null, "application/json", "application/json",
                header);
        JSONObject resJsonObj = JSONObject.parseObject(res);
        return resJsonObj.getJSONObject("data").getString("url");
    }

    @Async
    @Override
    public void doGetCameraData(String number, Integer delayTime) {
        try {
            logger.info("doGetCameraData, the number is " + number);
            if(concurrentHashMap.containsKey(number)) {
                logger.info("doGetCameraData: the number is taken picture, the number is " + number);
                return;
            }
            concurrentHashMap = new ConcurrentHashMap<>();
            logger.info("doGetCameraData, the number is{}, sleep time is:{} ",number, delayTime);
            Thread.sleep(1000L * delayTime);
            logger.info("doGetCameraData, start take picture, the number is {}", number);
            CarDataDao carDataDao = new CarDataDao();
            carDataDao.setNumber(number);
            carDataDao.setGmtCreated(LocalDateTime.now());
            carDataDao.setGmtModified(LocalDateTime.now());
            concurrentHashMap.put(number, new CarDataDao());
            Map<String, String> resultMap = new HashMap<>();
            Map<String, String> map = getCameraList(userId);
            System.out.println("cameraList: "+map);
            for(String camera: map.keySet()) {
                if(map.get(camera).equals(frontDetectOnlyCameraId) || map.get(camera).equals(endDetectOnlyCameraId)){
                    continue;
                }
                try {
                    String path = getCameraPic(userId, "", map.get(camera));
                    System.out.println(camera+": "+path);
                    resultMap.put(camera, getRealPath(path));
                } catch (Exception e) {
                    logger.error("doGetCameraData, take picture error, the number is {}, the camera is :{}, error:{}", number, camera, e.getMessage());
                }
            }
            carDataDao.setCameraDatas(resultMap);
            concurrentHashMap.put(number, carDataDao);
            logger.info("doGetCameraData, take picture success, the number is {}", number);
        } catch (Exception e) {
            e.printStackTrace();
            redisUtil.del(number);
        }
    }

    @Override
    public ApiResult doGetCameraDataByPerson(String number) {
        try {
            CarDataDao carDataDao = new CarDataDao();
            carDataDao.setNumber(number);
            carDataDao.setGmtCreated(LocalDateTime.now());
            carDataDao.setGmtModified(LocalDateTime.now());
            Map<String, String> resultMap = new HashMap<>();
            Map<String, String> map = getCameraList(userId);
            System.out.println("cameraList: "+map);
            for(String camera: map.keySet()) {
                if(map.get(camera).equals(frontDetectOnlyCameraId) || map.get(camera).equals(endDetectOnlyCameraId)){
                    continue;
                }
                try {
                    String path = getCameraPic(userId, "", map.get(camera));
                    System.out.println(camera+": "+path);
                    resultMap.put(camera, getRealPath(path));
                }catch (Exception e) {
                    logger.error("doGetCameraDataByPerson, take picture error, the number is {}, the camera is :{}, error:{}", number, camera, e.getMessage());
                }
            }
            carDataDao.setCameraDatas(resultMap);
            concurrentHashMap.put(number, carDataDao);
            return ApiResult.ofSuccess(carDataDao);
        } catch (Exception e) {
            e.printStackTrace();
            redisUtil.del(number);
            return ApiResult.ofFail(400,e.getMessage());
        }
    }

    @Override
    public ApiResult getCarData() {
        if(concurrentHashMap.isEmpty()) {
            String number = redisTemplateUtil.getKeyValue("car");
            if(Objects.isNull(number)) {
                return ApiResult.ofFail(400,"no data");
            }
            CarDataDao carDataDao = new CarDataDao();
            carDataDao.setNumber(number.substring(4));
            return ApiResult.ofSuccess(carDataDao);
        }
        for(String number: concurrentHashMap.keySet()) {
            return ApiResult.ofSuccess(concurrentHashMap.get(number));
        }
        return ApiResult.ofSuccess();
    }

    @Override
    public void removeCar() {
        logger.info("removeCar");
        concurrentHashMap = new ConcurrentHashMap<>();
        redisTemplateUtil.deleteKey("car");
    }
    public void removeCarByNumber() {
        logger.info("initCar");
        concurrentHashMap = new ConcurrentHashMap<>();
    }

    private String getRealPath(String path) {
        //变成绝对路径
        return prePath + path;
    }
}
