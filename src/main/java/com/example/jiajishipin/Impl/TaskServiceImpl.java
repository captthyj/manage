package com.example.jiajishipin.Impl;

import cn.hutool.json.JSONArray;
import com.example.jiajishipin.entity.CarEntity;
import com.example.jiajishipin.redis.RedisTemplateUtil;
import com.example.jiajishipin.service.CameraService;
import com.example.jiajishipin.service.TaskService;
import com.example.jiajishipin.utils.ApiResult;
import com.example.jiajishipin.utils.RedisUtil;

import cn.hutool.json.JSONObject;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class TaskServiceImpl implements TaskService {
    /**
     *@ClassName TaskServiceImpl
     *@Description TODO
     *@Author Hyj
     *@Date 2024/5/25 12:07
     *@Version 1.0
     **/

    private static  final Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    RedisTemplateUtil redisTemplateUtil;
    @Autowired
    CameraService cameraService;

    
    @Value("${front-camera-id}")
    private String frontCameraId;
    
    @Value("${end-camera-id}")
    private String endCameraId;
    
    @Value("${front-detect-only-camera-id}")
    private String frontDetectOnlyCameraId;

    @Value("${end-detect-only-camera-id}")
    private String endDetectOnlyCameraId;

    public Integer delayTime = 10;

    @Override
    public void saveData(String number) {
        //调用拍照接口，获取照片
        CarEntity carEntity = (CarEntity) redisUtil.get(number);
        if(Objects.isNull(carEntity)) {
            logger.error("save data failed, number is :{}" , number);
        }

    }

    @Override
    public void cameraOneEvent(String number) {
        //摄像头1 识别到车辆
        CarEntity carEntity ;
        System.out.println("1号摄像头识别到车辆："+ number);
        logger.info("1号摄像头识别到车辆:{}", number);
        if(StringUtil.isNullOrEmpty(number)) {
            return;
        }
        if(number.equals("UNKNOWN") || number.equals("unknown")){
            logger.info("1号摄像头识别到车辆:{},unknown 跳过", number);
            return;
        }
        if(number.contains("挂")) {
            logger.info("1号摄像头识别到车辆:{},带挂 跳过", number);
            return;
        }
        if(redisUtil.hasKey(number)) {
            logger.info("{}已经存在于拍照队列",number);
            return;
        } else if (redisTemplateUtil.getKey("data"+number)) {
            logger.info("{}已经存在于队列",number);
            redisTemplateUtil.deleteKey("data"+number);
            return;
        } else {
            redisTemplateUtil.setKeyValueTime("car", "data" + number, 300L, TimeUnit.SECONDS);
            redisTemplateUtil.setKeyValueTime("data"+ number, number, 600L, TimeUnit.SECONDS);
            carEntity = new CarEntity();
            carEntity.setNumber(number);
            carEntity.setCameraOne(true);
            carEntity.setGmtModified(LocalDateTime.now());
            carEntity.setGmtModified(LocalDateTime.now());
            redisUtil.set(number, carEntity, 600L);
            cameraService.doGetCameraData(number, this.delayTime);
        }
        System.out.println("one: "+number+" "+carEntity.toString());
    }

    @Override
    public void cameraTwoEvent(String number) {
        //摄像头2 识别到车辆
        CarEntity carEntity ;
        System.out.println("2号摄像头识别到车辆："+ number);
        logger.info("2号摄像头识别到车辆:{}", number);
        if(StringUtil.isNullOrEmpty(number)) {
            return;
        }
        if(number.equals("UNKNOWN") || number.equals("unknown")){
            logger.info("2号摄像头识别到车辆:{},unknown 跳过", number);
            return;
        }
        if(number.contains("挂")) {
            logger.info("2号摄像头识别到车辆:{},带挂 跳过", number);
            return;
        }
        if(redisUtil.hasKey(number)) {
            logger.info("{}已经存在于拍照队列",number);
            return;
        } else if (redisTemplateUtil.getKey("data"+ number)) {
            logger.info("{}已经存在于队列",number);
            redisTemplateUtil.deleteKey("data"+ number);
            return;
        } else {
            redisTemplateUtil.setKeyValueTime("car", "data" + number, 300L, TimeUnit.SECONDS);
            redisTemplateUtil.setKeyValueTime("data"+ number, number, 600L, TimeUnit.SECONDS);
            carEntity = new CarEntity();
            carEntity.setNumber(number);
            carEntity.setCameraTwo(true);
            carEntity.setGmtModified(LocalDateTime.now());
            carEntity.setGmtModified(LocalDateTime.now());
            redisUtil.set(number, carEntity, 600L);
            cameraService.doGetCameraData(number, this.delayTime);
        }
        System.out.println("two: "+number+" "+carEntity.toString());

    }

    @Override
    public void lineCrushingEvent(JSONObject data) {
        try{
            System.out.println(data);
            JSONArray events =  data.getJSONObject("params")
                    .getJSONArray("events");

            events.forEach(e->{
                JSONObject e_obj = (JSONObject)e;
                JSONObject vehicleRcogResult = e_obj
                        .getJSONObject("data")
                        .getJSONArray("vehicleRcogResult")
                        .getJSONObject(0);

                String cameraId = vehicleRcogResult
                        .getJSONObject("targetAttrs")
                        .getStr("cameraIndexCode");
                String number = vehicleRcogResult
                        .getJSONArray("target")
                        .getJSONObject(0)
                        .getJSONObject("vehicle")
                        .getJSONObject("plateNo")
                        .getStr("value");

                // if(cameraId.equals(frontCameraId)){
                //     cameraOneEvent(number);
                // }else if(cameraId.equals(endCameraId)){
                //     cameraTwoEvent(number);
                // }
                if(Arrays.asList(
                    frontCameraId, 
                    endCameraId, 
                    frontDetectOnlyCameraId, 
                    endDetectOnlyCameraId
                    )   
                    .contains(cameraId)
                ){
                    cameraTwoEvent(number);
                }
            });
        }catch(Exception e){
            System.out.println(data);
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        redisTemplateUtil.deleteKey("car");
    }

    @Override
    public ApiResult getDelayTime() {
        return ApiResult.ofSuccess(delayTime);
    }

    @Override
    public ApiResult setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
        return ApiResult.ofSuccess(delayTime);
    }
}
