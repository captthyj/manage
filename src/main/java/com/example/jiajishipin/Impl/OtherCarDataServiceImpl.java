package com.example.jiajishipin.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jiajishipin.dao.OtherCarDataDao;
import com.example.jiajishipin.dao.ImageDataDao;
import com.example.jiajishipin.mapper.OtherCarDataMapper;
import com.example.jiajishipin.service.CameraService;
import com.example.jiajishipin.service.OtherCarDataService;
import com.example.jiajishipin.service.ImageDataService;
import com.example.jiajishipin.utils.ApiResult;
import com.example.jiajishipin.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.net.ssl.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Objects;

/**
 *@ClassName OtherCarDataServiceImpl
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 21:35
 *@Version 1.0
 **/
@Service
public class OtherCarDataServiceImpl extends ServiceImpl<OtherCarDataMapper, OtherCarDataDao> implements OtherCarDataService {
    @Autowired
    OtherCarDataMapper otherCarDataMapper;
    @Value("${base-path}")
    public String basePath;
    @Value("${pre-path}")
    public String prePath;
    @Autowired
    ImageDataService imageDataService;
    @Autowired
    CameraService cameraService;

    public OtherCarDataServiceImpl() throws Exception {
        trustAllCerts();
    }

    @Override
    public ApiResult getOtherCarData(LocalDateTime fromDate, LocalDateTime toDate, String applyPart,String name, String unit, String count, String goodsLocation,String isChargeGoods, String notes, int currentPage, int pageSize){
        QueryWrapper<OtherCarDataDao> queryWrapper = new QueryWrapper<>();
        if(!Objects.isNull(fromDate)) {
            queryWrapper.lambda().ge(OtherCarDataDao::getGmtCreated, fromDate);
        }
        if(!Objects.isNull(toDate)) {
            queryWrapper.lambda().le(OtherCarDataDao::getGmtCreated, toDate);
        }
//        if(!Objects.isNull(applyPart) && !applyPart.isEmpty()) {
//            queryWrapper.lambda().eq(OtherCarDataDao::getApplyPart, applyPart);
//        }
//        if(!Objects.isNull(name) && !name.isEmpty()) {
//            queryWrapper.lambda().eq(OtherCarDataDao::getName,name);
//        }
//        if(!Objects.isNull(unit) && !unit.isEmpty()) {
//            queryWrapper.lambda().eq(OtherCarDataDao::getUnit,unit);
//        }
//        if(!Objects.isNull(count) && !count.isEmpty()) {
//            queryWrapper.lambda().eq(OtherCarDataDao::getCount,count);
//        }
//        if(!Objects.isNull(goodsLocation) && !goodsLocation.isEmpty()) {
//            queryWrapper.lambda().eq(OtherCarDataDao::getGoodsLocation,goodsLocation);
//        }
//        if(!Objects.isNull(isChargeGoods) && !isChargeGoods.isEmpty()) {
//            queryWrapper.lambda().eq(OtherCarDataDao::getIsChargeGoods,isChargeGoods);
//        }
//        if(!Objects.isNull(notes) && !notes.isEmpty()) {
//            queryWrapper.lambda().eq(OtherCarDataDao::getNotes,notes);
//        }
        queryWrapper.orderByDesc("id");
        IPage<OtherCarDataDao> page = new Page<>(currentPage, pageSize);
        otherCarDataMapper.selectPage(page, queryWrapper);
        return ApiResult.ofSuccess(page);
    }

    @Override
    public Boolean saveOtherCarData(OtherCarDataDao carDataDao) throws IOException {
        LocalDateTime localDateTime = LocalDateTime.now();
        Integer year = localDateTime.getYear();
        Integer month = localDateTime.getMonthValue();
        Integer day = localDateTime.getDayOfMonth();
        carDataDao.setYear(year);
        carDataDao.setMonth(month);
        carDataDao.setDay(day);
        carDataDao.setFilePath("otherCarData/" + year.toString() + "/" + month.toString() + "/" + day.toString() + "/"
                + carDataDao.getNumber() + "_" + TimeUtil.getTime(localDateTime).replace(":", "_")+ "/");
        otherCarDataMapper.insert(carDataDao);
        OtherCarDataDao newOtherCarDataDao = otherCarDataMapper.selectOne(new QueryWrapper<OtherCarDataDao>().lambda().eq(OtherCarDataDao::getFilePath, carDataDao.getFilePath()));
        for(String s: carDataDao.getCameraDatas().keySet()){
            String fromUrl = carDataDao.getCameraDatas().get(s);
            // 文件名是摄像头的名字
            String fileUrl = carDataDao.getFilePath()+s.replace("#", "-")+".jpeg";
            String toPath  = savePictures(fromUrl, basePath + "/" + fileUrl);
            carDataDao.getCameraDatas().put(s, fileUrl);
            ImageDataDao imageDataDao = new ImageDataDao();
            imageDataDao.setCarDataId(newOtherCarDataDao.getId());
            imageDataDao.setFilePath(toPath);
            imageDataDao.setData(Files.readAllBytes(Paths.get(toPath)));
            imageDataDao.setGmtModified(LocalDateTime.now());
            imageDataDao.setGmtCreated(LocalDateTime.now());
            imageDataDao.setNumber(newOtherCarDataDao.getNumber());
            imageDataDao.setCameraNumber(s);
            imageDataService.save(imageDataDao);
        }
        File file = saveScannerPictures(carDataDao.getScannerImage(), basePath + "/" + carDataDao.getFilePath()+"scanner.jpeg");
        carDataDao.setScannerImage(carDataDao.getFilePath()+"scanner.jpeg");
        ImageDataDao imageDataDao = new ImageDataDao();
        imageDataDao.setCarDataId(newOtherCarDataDao.getId());
        imageDataDao.setFilePath(file.getAbsolutePath());
        imageDataDao.setData(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
        imageDataDao.setGmtModified(LocalDateTime.now());
        imageDataDao.setGmtCreated(LocalDateTime.now());
        imageDataDao.setNumber(newOtherCarDataDao.getNumber());
        imageDataDao.setCameraNumber("scanner");
        imageDataService.save(imageDataDao);
        otherCarDataMapper.updateById(carDataDao);
        cameraService.removeCar();
        return true ;
    }
    public static String encodeImageToBase64(File imageFile) throws IOException {
        try (FileInputStream fis = new FileInputStream(imageFile)) {
//            byte[] imageBytes = fis.readAllBytes();
//            return Base64.getEncoder().encodeToString(imageBytes);
            return null;
        }
    }


    public static void trustAllCerts() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = (hostname, session) -> true;

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    public String savePictures(String fromUrl, String toPath) throws IOException {
        // 创建文件路径（如果不存在）
        File directory = new File(toPath).getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            return "Failed to create directory: " + directory;
        }
        // 下载文件
        try (BufferedInputStream bis = new BufferedInputStream(new URL(fromUrl).openStream());
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(toPath))) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            return toPath;
        }
    }
    public File saveScannerPictures(String base64, String filePath) throws IOException {
        try {
            byte[] imageBytes = Base64.getDecoder().decode(base64.replaceFirst("data:image/jpeg;base64,", ""));
            // 使用ImageIO读取字节数组
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageBytes));

            File outputFile = new File(filePath);
            ImageIO.write(image, "jpg", outputFile);
            return outputFile;
        } catch (Exception e) {
            throw e;
        }
    }

}
