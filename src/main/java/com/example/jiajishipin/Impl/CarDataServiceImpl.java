package com.example.jiajishipin.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jiajishipin.dao.ImageDataDao;
import com.example.jiajishipin.dao.OtherCarDataDao;
import com.example.jiajishipin.mapper.CarDataMapper;
import com.example.jiajishipin.service.CameraService;
import com.example.jiajishipin.service.CarDataService;
import com.example.jiajishipin.service.ImageDataService;
import com.example.jiajishipin.utils.ApiResult;
import com.example.jiajishipin.dao.CarDataDao;
import com.example.jiajishipin.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.net.ssl.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Objects;

/**
 *@ClassName CarDataServiceImpl
 *@Description TODO
 *@Author Hyj
 *@Date 2024/5/26 21:35
 *@Version 1.0
 **/
@Service
public class CarDataServiceImpl extends ServiceImpl<CarDataMapper, CarDataDao> implements CarDataService {
    @Autowired
    CarDataMapper carDataMapper;
    @Value("${base-path}")
    public String basePath;
    @Value("${pre-path}")
    public String prePath;
    @Autowired
    ImageDataService imageDataService;
    @Autowired
    CameraService cameraService;

    public CarDataServiceImpl() throws Exception {
        trustAllCerts();
    }
    @Override
    public ApiResult getCarDataByNumber(LocalDateTime fromDate, LocalDateTime toDate,String number, int currentPage, int pageSize){
        QueryWrapper<CarDataDao> queryWrapper = new QueryWrapper<>();
        if(!Objects.isNull(fromDate)) {
            queryWrapper.lambda().ge(CarDataDao::getGmtCreated, fromDate);
        }
        if(!Objects.isNull(toDate)) {
            queryWrapper.lambda().le(CarDataDao::getGmtCreated, toDate);
        }
        if(!Objects.isNull(number) && !number.isEmpty()) {
            queryWrapper.lambda().eq(CarDataDao::getNumber, number);
        }
        queryWrapper.orderByDesc("id");
        IPage<CarDataDao> page = new Page<>(currentPage, pageSize);
        carDataMapper.selectPage(page, queryWrapper);
        return ApiResult.ofSuccess(page);
    }

    @Override
    public Boolean saveCarData(CarDataDao carDataDao) throws IOException {
        LocalDateTime localDateTime = LocalDateTime.now();
        Integer year = localDateTime.getYear();
        Integer month = localDateTime.getMonthValue();
        Integer day = localDateTime.getDayOfMonth();
        carDataDao.setYear(year);
        carDataDao.setMonth(month);
        carDataDao.setDay(day);
        carDataDao.setFilePath("carData/" + year.toString() + "/" + month.toString() + "/" + day.toString() + "/"
                + carDataDao.getNumber() + "_" + TimeUtil.getTime(localDateTime).replace(":", "_")+ "/");
        carDataMapper.insert(carDataDao);
        CarDataDao newCarDataDao = carDataMapper.selectOne(new QueryWrapper<CarDataDao>().lambda().eq(CarDataDao::getFilePath, carDataDao.getFilePath()));
        for(String s: carDataDao.getCameraDatas().keySet()){
            String fromUrl = carDataDao.getCameraDatas().get(s);
            // 文件名是摄像头的名字
            String fileUrl = carDataDao.getFilePath()+s.replace("#", "-")+".jpeg";
            String toPath  = savePictures(fromUrl, basePath + "/" + fileUrl);
            newCarDataDao.getCameraDatas().put(s, fileUrl);
            ImageDataDao imageDataDao = new ImageDataDao();
            imageDataDao.setCarDataId(newCarDataDao.getId());
            imageDataDao.setFilePath(toPath);
            imageDataDao.setData(Files.readAllBytes(Paths.get(toPath)));
            imageDataDao.setGmtModified(LocalDateTime.now());
            imageDataDao.setGmtCreated(LocalDateTime.now());
            imageDataDao.setNumber(newCarDataDao.getNumber());
            imageDataDao.setCameraNumber(s);
            imageDataService.save(imageDataDao);
        }
        File file = saveScannerPictures(carDataDao.getScannerImage(), basePath + "/" + carDataDao.getFilePath()+"scanner.jpeg");
        newCarDataDao.setScannerImage(carDataDao.getFilePath()+"scanner.jpeg");
        ImageDataDao imageDataDao = new ImageDataDao();
        imageDataDao.setCarDataId(newCarDataDao.getId());
        imageDataDao.setFilePath(file.getAbsolutePath());
        imageDataDao.setData(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
        imageDataDao.setGmtModified(LocalDateTime.now());
        imageDataDao.setGmtCreated(LocalDateTime.now());
        imageDataDao.setNumber(newCarDataDao.getNumber());
        imageDataDao.setCameraNumber("scanner");
        imageDataService.save(imageDataDao);
        carDataMapper.updateById(newCarDataDao);
        cameraService.removeCar();
        return true ;
    }
    public static String encodeImageToBase64(File imageFile) throws IOException {
        try (FileInputStream fis = new FileInputStream(imageFile)) {
//            byte[] imageBytes = fis.readAllBytes();
//            return Base64.getEncoder().encodeToString(imageBytes);
            return null;
        }
    }


    public static void trustAllCerts() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = (hostname, session) -> true;

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    public String savePictures(String fromUrl, String toPath) throws IOException {
        // 创建文件路径（如果不存在）
        File directory = new File(toPath).getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            return "Failed to create directory: " + directory;
        }
        // 下载文件
        try (BufferedInputStream bis = new BufferedInputStream(new URL(fromUrl).openStream());
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(toPath))) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            return toPath;
        }
    }
    public File saveScannerPictures(String base64, String filePath) throws IOException {
        try {
            byte[] imageBytes = Base64.getDecoder().decode(base64.replaceFirst("data:image/jpeg;base64,", ""));
            // 使用ImageIO读取字节数组
            BufferedImage image = ImageIO.read(new java.io.ByteArrayInputStream(imageBytes));

            File outputFile = new File(filePath);
            ImageIO.write(image, "jpg", outputFile);
            return outputFile;
        } catch (Exception e) {
            throw e;
        }
    }

}
