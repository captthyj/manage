package com.example.jiajishipin.exception;

import java.text.MessageFormat;
import java.util.Optional;

/*
 * @Author Hyj
 * @Description //TODO
 * @Date  2023/2/18 16:13
 **/
public interface IErrorCode {
    /**
     * 获取code
     *
     * @return
     */
    int getCode();

    /**
     * 获取message
     *
     * @return
     */
    String getMessage();

    /**
     * 格式化返回信息
     *
     * @param params 参数
     * @return
     */
    default String formatMsg(Object... params) {
        if (Optional.ofNullable(params).isPresent()) {
            return MessageFormat.format(getMessage(), params);
        }
        return getMessage();

    }


}
