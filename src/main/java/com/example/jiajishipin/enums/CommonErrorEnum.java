package com.example.jiajishipin.enums;


import com.example.jiajishipin.exception.IErrorCode;

/*
 * @Author Hyj
 * @Description //TODO
 * @Date  2023/2/18 16:13
 **/
public enum CommonErrorEnum implements IErrorCode {
    SUCCESS(200, "成功"),
    REQUEST_PARAMS_NOT_VALID_ERROR(201,  "请求参数[{0}]无效"),
    EXCEPTION(500, "服务器开小差，请稍后再试"),
    INTERNAL_SERVER_ERROR_ARGS(999, "服务端异常: {0}"),
    ILLEGAL_REQUEST(508, "非法请求"),
    NO_ACCESS_VISIT(509,"无权限访问"),
    EVENT_HANDLER_ERROR(510,"数据库处理错误"),
    USER_IS_NOT_LOGIN(511,"用户未登录");
    private final int code;

    private final String message;

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    CommonErrorEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
